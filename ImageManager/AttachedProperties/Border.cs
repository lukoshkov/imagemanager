﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ImageManager.AttachedProperties
{
	public class Border
	{
		public static bool GetAutoCornerRadius(DependencyObject obj)
		{
			return (bool)obj.GetValue(AutoCornerRadiusProperty);
		}

		public static void SetAutoCornerRadius(DependencyObject obj, bool value)
		{
			obj.SetValue(AutoCornerRadiusProperty, value);
		}

		public static readonly DependencyProperty AutoCornerRadiusProperty =
			DependencyProperty.RegisterAttached("AutoCornerRadius", typeof(bool), typeof(Border), new PropertyMetadata(false, OnAutoCornerChanged));

		private static void OnAutoCornerChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
		{
			var element = d as FrameworkElement;
			if (element == null)
			{
				return;
			}
			element.SizeChanged -= Element_SizeChanged;
			if ((bool)e.NewValue)
			{
				CalcAutoCornerRadius(element);
				element.SizeChanged += Element_SizeChanged;
			}
			else
			{
				SetCornerRadius(element, GetFallbackCornerRadius(element));
			}
		}

		private static void Element_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			var element = sender as FrameworkElement;
			if (element == null)
			{
				return;
			}
			CalcAutoCornerRadius(element);
		}

		private static void CalcAutoCornerRadius(FrameworkElement element)
		{
			var radius = element.ActualHeight / 2;
			SetCornerRadius(element, new CornerRadius(radius));
		}


		public static CornerRadius GetCornerRadius(DependencyObject obj)
		{
			return (CornerRadius)obj.GetValue(CornerRadiusProperty);
		}

		public static void SetCornerRadius(DependencyObject obj, CornerRadius value)
		{
			obj.SetValue(CornerRadiusProperty, value);
		}
		
		public static readonly DependencyProperty CornerRadiusProperty =
			DependencyProperty.RegisterAttached("CornerRadius", typeof(CornerRadius), typeof(Border), new PropertyMetadata(new CornerRadius()));



		public static CornerRadius GetFallbackCornerRadius(DependencyObject obj)
		{
			return (CornerRadius)obj.GetValue(FallbackCornerRadiusProperty);
		}

		public static void SetFallbackCornerRadius(DependencyObject obj, CornerRadius value)
		{
			obj.SetValue(FallbackCornerRadiusProperty, value);
		}
		
		public static readonly DependencyProperty FallbackCornerRadiusProperty =
			DependencyProperty.RegisterAttached("FallbackCornerRadius", typeof(CornerRadius), typeof(Border), new PropertyMetadata(new CornerRadius()));


	}
}
