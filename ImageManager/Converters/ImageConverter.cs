﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ImageManager.Converters
{
	[ValueConversion(typeof(string), typeof(ImageSource))]
	public class ImageConverter : IValueConverter
	{
		private Utility.ImageCache _cache;
		private Utility.DiskImageCache _disk;
		private PathToIconConverter _iconConverter;

		public int ImageDecodeWidth { get; set; }
		public int ImageDecodeHeight { get; set; }
		public bool IsCached { get; set; }
		public bool IsDiskCacheEnabled { get; set; }

		public ImageConverter()
		{
			_cache = Utility.ImageCache.Instance;
			_disk = Utility.DiskImageCache.Instance;
			_iconConverter = new PathToIconConverter();
			_iconConverter.Size = Utility.IconUtil.IconSize.Jumbo;
			IsCached = true;
		}

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var path = (string)value;
			ImageSource image;
			if (IsCached && _cache.TryGetValue(ImageDecodeWidth, ImageDecodeHeight, path, out image))
			{
				return image;
			}
			BitmapImage bi;
			bool loaded = false;
			if (IsDiskCacheEnabled)
			{
				loaded = _disk.TryLoadCached(path, out bi, ImageDecodeWidth, ImageDecodeHeight);
			}
			else
			{
				loaded = Utility.DiskImageCache.TryLoadBitmap(path, out bi, ImageDecodeWidth, ImageDecodeHeight);
			}
			if (!loaded)
			{
				return _iconConverter.Convert(value, targetType, parameter, culture);
			}
			if (IsCached)
			{
				_cache.TryAdd(ImageDecodeWidth, ImageDecodeHeight, path, bi);
			}
			return bi;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
