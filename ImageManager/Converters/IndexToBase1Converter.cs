﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ImageManager.Converters
{
	[ValueConversion(typeof(int), typeof(int))]
	public class IndexToBase1Converter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is int)
			{
				return (int)value + 1;
			}
			return Binding.DoNothing;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is int)
			{
				return (int)value - 1;
			}
			return Binding.DoNothing;
		}
	}
}
