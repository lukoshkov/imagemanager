﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ImageManager.Converters
{
	[ValueConversion(typeof(int), typeof(int[]))]
	public class PageCountToNumbersConverter : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is int)
			{
				var indexes = new int[Math.Max((int)value, 1)];
				for (int i = 0; i < indexes.Length; i++)
				{
					indexes[i] = i + 1;
				}
				return indexes;
			}
			return Binding.DoNothing;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
