﻿using ImageManager.Utility;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace ImageManager.Converters
{
	[ValueConversion(typeof(string), typeof(ImageSource))]
	public class PathToIconConverter : IValueConverter
	{
		private ImageCache _cache = ImageCache.Instance;

		public IconUtil.IconSize Size { get; set; } = IconUtil.IconSize.Jumbo;

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			var path = (string)value;
			if (!new System.IO.FileInfo(path).Attributes.HasFlag(System.IO.FileAttributes.Directory))
			{
				path = "*" + System.IO.Path.GetExtension(path).ToLower();
			}
			var size = (int)Size;
			ImageSource image;
			if (_cache.TryGetValue(size, size, path, out image))
			{
				return image;
			}
			try
			{
				image = IconUtil.GetFileIconBitmap(path, Size);
				_cache.TryAdd(size, size, path, image);
				return image;
			}
			catch { return null; }
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}
	}
}
