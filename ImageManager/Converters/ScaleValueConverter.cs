﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace ImageManager.Converters
{
	[ValueConversion(typeof(bool), typeof(object))]
	public class ScaleValueConverter : IValueConverter
	{
		public double ScaleFactor { get; set; } = 1;

		public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is double)
			{
				return ScaleFactor * (double)value;
			}
			return Binding.DoNothing;
		}

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
		{
			if (value is double && ScaleFactor != 0)
			{
				return (double)value / ScaleFactor;
			}
			return Binding.DoNothing;
		}
	}
}
