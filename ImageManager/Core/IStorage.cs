﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManager.Core
{
	public interface IStorage
	{
		Tag AddNewTag(string tagTitle);
		List<Tag> AddNewTags(IEnumerable<string> tagTitles);
		TaggedImage AddNewImage(string imageFilePath);
		TaggedImage AddNewImage(string imageFilePath, List<Tag> tags);
		TaggedImage AddNewImage(string imageFilePath, List<Tag> tags, bool isFavourite);
		List<TaggedImage> AddNewImages(IEnumerable<TaggedImage> imageTemplates);

		void UpdateTag(Tag tag);
		void UpdateTags(IEnumerable<Tag> tags);
		void UpdateImage(TaggedImage image);
		void UpdateImages(IEnumerable<TaggedImage> images);

		void DeleteTag(Tag tag);
		void DeleteImage(TaggedImage image);

		List<Tag> GetAllTags();
		List<TaggedImage> GetAllImages();
		List<TaggedImage> GetImagesWithTags(List<Tag> tags);
	}
}
