﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManager.Core
{
	public class Tag
	{
		public long ID { get; set; }
		public string Title { get; set; }
		public List<Tag> RelatedTags { get; protected set; }

		public Tag()
		{
			RelatedTags = new List<Tag>();
		}

		public Tag(long id, string title)
			: this()
		{
			ID = id;
			Title = title;
		}

		public static bool IsTitleValid(string title)
		{
			const string invalidChars = ";,./\\\"'#";
			return !string.IsNullOrWhiteSpace(title) && !title.Any(c => invalidChars.Contains(c));
		}
	}
}
