﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManager.Core
{
	public class TagComparer : IComparer<Tag>
	{
		public int Compare(Tag x, Tag y)
		{
			if (x == null && y == null) return 0;
			if (x == null) return -1;
			if (y == null) return 1;
			return x.Title.CompareTo(y.Title);
		}
	}
}
