﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManager.Core
{
	public class TaggedImage
	{
		public long ID { get; set; }
		public string FilePath { get; set; }
		public List<Tag> Tags { get; protected set; }
		public bool IsFavourite { get; set; }

		public TaggedImage()
		{
			Tags = new List<Tag>();
		}

		public TaggedImage(long id, string filePath)
			: this()
		{
			ID = id;
			FilePath = filePath;
		}

		public TaggedImage(long id, string filePath, bool isFavourite)
			: this()
		{
			ID = id;
			FilePath = filePath;
			IsFavourite = isFavourite;
		}
	}
}
