﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManager.Core
{
	public class TaggedImageComparer : IComparer<TaggedImage>
	{
		public int Compare(TaggedImage x, TaggedImage y)
		{
			if (x == null && y == null) return 0;
			if (x == null) return -1;
			if (y == null) return 1;
			var score = (y.IsFavourite ? 1 : 0) - (x.IsFavourite ? 1 : 0);
			if (score != 0)
			{
				return score;
			}
			return x.FilePath.CompareTo(y.FilePath);
		}
	}
}
