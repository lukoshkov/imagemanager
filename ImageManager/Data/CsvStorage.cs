﻿using ImageManager.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManager.Data
{
	public class CsvStorage : IStorage
	{
		private string _tagsPath;
		private string _imagesPath;

		private List<Tag> _tags;
		private List<TaggedImage> _images;

		public CsvStorage(string tagsCsvPath, string imagesCsvPath)
		{
			_tagsPath = tagsCsvPath;
			_imagesPath = imagesCsvPath;
			CreateFilesIfNotExist();
		}

		public TaggedImage AddNewImage(string imageFilePath)
		{
			return AddNewImage(imageFilePath, null);
		}

		public TaggedImage AddNewImage(string imageFilePath, List<Tag> tags)
		{
			return AddNewImage(imageFilePath, tags, false);
		}

		public TaggedImage AddNewImage(string imageFilePath, List<Tag> tags, bool isFavourite)
		{
			long newID = GetMaxID(_imagesPath) + 1;
			var image = new TaggedImage(newID, imageFilePath, isFavourite);
			if (tags != null)
			{
				image.Tags.AddRange(tags);
			}
			using (var csv = new System.IO.StreamWriter(_imagesPath, true))
			{
				csv.WriteLine(TaggedImageToLine(image));
			}
			if (_images != null)
			{
				_images.Add(image);
			}
			return image;
		}

		public List<TaggedImage> AddNewImages(IEnumerable<TaggedImage> imageTemplates)
		{
			var images = new List<TaggedImage>();
			foreach (var template in imageTemplates)
			{
				images.Add(AddNewImage(template.FilePath, template.Tags, template.IsFavourite));
			}
			return images;
		}

		public Tag AddNewTag(string tagTitle)
		{
			long newID = GetMaxID(_tagsPath) + 1;
			var tag = new Tag(newID, tagTitle);
			using (var csv = new System.IO.StreamWriter(_tagsPath, true))
			{
				csv.WriteLine(TagToLine(tag));
			}
			if (_tags != null)
			{
				_tags.Add(tag);
			}
			return tag;
		}

		public List<Tag> AddNewTags(IEnumerable<string> tagTitles)
		{
			var tags = new List<Tag>();
			foreach (var title in tagTitles)
			{
				tags.Add(AddNewTag(title));
			}
			return tags;
		}

		public List<TaggedImage> GetAllImages()
		{
			if (_images == null)
			{
				GetAllTags();
				_images = new List<TaggedImage>();
				long id;
				int isFavourite;
				using (var csv = new System.IO.StreamReader(_imagesPath))
				{
					while (!csv.EndOfStream)
					{
						var line = csv.ReadLine().Split(';');
						if (line.Length >= 3 && !string.IsNullOrWhiteSpace(line[1])
							&& long.TryParse(line[0], out id) && int.TryParse(line[1], out isFavourite))
						{
							var image = new TaggedImage(id, line[2], isFavourite > 0);
							for (int i = 2; i < line.Length; i++)
							{
								if (long.TryParse(line[i], out id))
								{
									var tag = _tags.Find(t => t.ID == id);
									if (tag != null)
									{
										image.Tags.Add(tag);
									}
								}
							}
							_images.Add(image);
						}
					}
				}
			}
			return _images;
		}

		public List<Tag> GetAllTags()
		{
			if (_tags == null)
			{
				_tags = new List<Tag>();
				long id;
				using (var csv = new System.IO.StreamReader(_tagsPath))
				{
					while (!csv.EndOfStream)
					{
						var line = csv.ReadLine().Split(';');
						if (line.Length == 2 && !string.IsNullOrWhiteSpace(line[1]) && long.TryParse(line[0], out id))
						{
							_tags.Add(new Tag(id, line[1]));
						}
					}
				}
			}
			return _tags;
		}

		public List<TaggedImage> GetImagesWithTags(List<Tag> tags)
		{
			GetAllImages();
			var result = _images.Where(image => tags.All(tag => image.Tags.Exists(t => t.ID == tag.ID)));
			return result.ToList();
		}

		public void UpdateImage(TaggedImage image)
		{
			UpdateLine(line => IsImageLineMatching(line, image), TaggedImageToLine(image), _imagesPath);
		}
		
		public void UpdateImages(IEnumerable<TaggedImage> images)
		{
			foreach (var image in images)
			{
				UpdateImage(image);
			}
		}

		public void UpdateTag(Tag tag)
		{
			UpdateLine(line => IsTagLineMatching(line, tag), TagToLine(tag), _tagsPath);
		}

		public void UpdateTags(IEnumerable<Tag> tags)
		{
			foreach (var tag in tags)
			{
				UpdateTag(tag);
			}
		}

		public void DeleteImage(TaggedImage image)
		{
			UpdateLine(line => IsImageLineMatching(line, image), null, _imagesPath);
		}

		public void DeleteTag(Tag tag)
		{
			UpdateLine(line => IsTagLineMatching(line, tag), null, _tagsPath);
			GetAllImages();
			foreach (var image in _images)
			{
				if (image.Tags.Contains(tag))
				{
					image.Tags.Remove(tag);
					UpdateImage(image);
				}
			}
		}

		private void CreateFilesIfNotExist()
		{
			if (!System.IO.File.Exists(_tagsPath))
			{
				using (System.IO.File.Create(_tagsPath))
				{ }
			}
			if (!System.IO.File.Exists(_imagesPath))
			{
				using (System.IO.File.Create(_imagesPath))
				{ }
			}
		}

		private static bool IsImageLineMatching(string line, TaggedImage image)
		{
			long id;
			var parts = line.Split(';');
			return parts.Length > 0 && long.TryParse(parts[0], out id) && id == image.ID;
		}

		private static bool IsTagLineMatching(string line, Tag tag)
		{
			long id;
			var parts = line.Split(';');
			return parts.Length > 0 && long.TryParse(parts[0], out id) && id == tag.ID;
		}

		private static long GetMaxID(string csvPath)
		{
			long id, maxID = -1;
			using (var csv = new System.IO.StreamReader(csvPath))
			{
				while (!csv.EndOfStream)
				{
					var line = csv.ReadLine().Split(';');
					if (line.Length > 0 && long.TryParse(line[0], out id))
					{
						if (id > maxID)
						{
							maxID = id;
						}
					}
				}
			}
			return maxID;
		}

		private static void UpdateLine(Func<string, bool> match, string newValue, string filePath)
		{
			var tempPath = filePath + ".temp";
			using (var reader = new System.IO.StreamReader(filePath))
			using (var writer = new System.IO.StreamWriter(tempPath))
			{
				while (!reader.EndOfStream)
				{
					var line = reader.ReadLine();
					if (match(line))
					{
						line = newValue;
					}
					if (line != null)
					{
						writer.WriteLine(line);
					}
				}
			}
			var backPath = filePath + ".bak";
			System.IO.File.Delete(backPath);
			System.IO.File.Move(filePath, backPath);
			System.IO.File.Move(tempPath, filePath);
		}

		private static string TagToLine(Tag tag)
		{
			return string.Format("{0};{1}", tag.ID, tag.Title);
		}

		private static string TaggedImageToLine(TaggedImage image)
		{
			var sb = new StringBuilder();
			sb.AppendFormat("{0};{1};{2}", image.ID, image.IsFavourite ? 1 : 0, image.FilePath);
			foreach (var tag in image.Tags)
			{
				sb.AppendFormat(";{0}", tag.ID);
			}
			return sb.ToString();
		}
	}
}
