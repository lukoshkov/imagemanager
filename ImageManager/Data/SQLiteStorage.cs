﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ImageManager.Core;
using System.Data.SQLite;

namespace ImageManager.Data
{
	public class SQLiteStorage : IStorage
	{
		private static readonly long LONG_TRUE = 1;
		private static readonly long LONG_FALSE = 0;

		private string _connectionString;
		private List<Tag> _tags;
		private List<TaggedImage> _images;

		public SQLiteStorage(string dbFilePath)
		{
			var csb = new SQLiteConnectionStringBuilder();
			csb.DataSource = dbFilePath;
			csb.ForeignKeys = true;
			_connectionString = csb.ConnectionString;
			if (!System.IO.File.Exists(dbFilePath))
			{
				SQLiteConnection.CreateFile(dbFilePath);
				using (var connection = new SQLiteConnection(_connectionString))
				{
					var createTables = new string[]
					{
						"CREATE TABLE IF NOT EXISTS [Tags] ("
						+ "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
						+ "[title] TEXT NOT NULL)",

						"CREATE TABLE IF NOT EXISTS [Images] ("
						+ "[id] INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "
						+ "[is_favourite] INTEGER, "
						+ "[filepath] TEXT)",

						"CREATE TABLE IF NOT EXISTS [TagsOfImages] ("
						+ "[image_id] INTEGER, "
						+ "[tag_id] INTEGER, "
						+ "FOREIGN KEY(image_id) REFERENCES Images(id) ON DELETE CASCADE, "
						+ "FOREIGN KEY(tag_id) REFERENCES Tags(id) ON DELETE CASCADE)",

						"CREATE TABLE IF NOT EXISTS [RelatedTags] ("
						+ "[parent_id] INTEGER, "
						+ "[child_id] INTEGER, "
						+ "FOREIGN KEY(parent_id) REFERENCES Tags(id) ON DELETE CASCADE, "
						+ "FOREIGN KEY(child_id) REFERENCES Tags(id) ON DELETE CASCADE)"
					};

					connection.Open();
					foreach (var text in createTables)
					{
						var command = new SQLiteCommand(text, connection);
						command.ExecuteNonQuery();
					}
					connection.Close();
				}
			}
		}

		public TaggedImage AddNewImage(string imageFilePath)
		{
			return AddNewImage(imageFilePath, null);
		}

		public TaggedImage AddNewImage(string imageFilePath, List<Tag> tags)
		{
			return AddNewImage(imageFilePath, tags, false);
		}

		public TaggedImage AddNewImage(string imageFilePath, List<Tag> tags, bool isFavourite)
		{
			TaggedImage image = null;
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				image = InsertImageCommand(connection, imageFilePath, isFavourite);
				connection.Close();
			}
			UpdateImageTags(image, tags, null);
			if (_images != null)
			{
				_images.Add(image);
			}
			return image;
		}

		public List<TaggedImage> AddNewImages(IEnumerable<TaggedImage> imageTemplates)
		{
			var images = new List<TaggedImage>();
			TaggedImage image;
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				try
				{
					foreach (var template in imageTemplates)
					{
						image = InsertImageCommand(connection, template.FilePath, template.IsFavourite);
						UpdateImageTags(connection, image, template.Tags, null);
						images.Add(image);
					}
					transaction.Commit();
				}
				catch (Exception)
				{
					transaction.Rollback();
				}
				connection.Close();
			}
			if (_images != null)
			{
				_images.AddRange(images);
			}
			return images;
		}

		public Tag AddNewTag(string tagTitle)
		{
			Tag tag = null;
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				tag = InsertTagCommand(connection, tagTitle);
				connection.Close();
			}
			if (_tags != null)
			{
				_tags.Add(tag);
			}
			return tag;
		}

		public List<Tag> AddNewTags(IEnumerable<string> tagTitles)
		{
			var tags = new List<Tag>();
			Tag tag;
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				try
				{
					foreach (var title in tagTitles)
					{
						tag = InsertTagCommand(connection, title);
						tags.Add(tag);
					}
					transaction.Commit();
				}
				catch (Exception)
				{
					transaction.Rollback();
				}
				connection.Close();
			}
			if (_tags != null)
			{
				_tags.AddRange(tags);
			}
			return tags;
		}

		public void DeleteImage(TaggedImage image)
		{
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				DeleteImageCommand(connection, image);
				connection.Close();
			}
		}

		public void DeleteTag(Tag tag)
		{
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				DeleteTagCommand(connection, tag);
				connection.Close();
			}
		}

		public List<TaggedImage> GetAllImages()
		{
			if (_images == null)
			{
				GetAllTags();
				_images = new List<TaggedImage>();
				using (var connection = new SQLiteConnection(_connectionString))
				{
					connection.Open();
					var text = "SELECT id, is_favourite, filepath FROM Images;";
					using (var command = new SQLiteCommand(text, connection))
					{
						using (var reader = command.ExecuteReader())
						{
							while (reader.Read())
							{
								var isFavourite = reader.GetInt64(1);
								var image = new TaggedImage(reader.GetInt64(0), reader.GetString(2), isFavourite == LONG_TRUE);
								_images.Add(image);
							}
						}
					}
					text = "SELECT image_id, tag_id FROM TagsOfImages;";
					using (var command = new SQLiteCommand(text, connection))
					{
						using (var reader = command.ExecuteReader())
						{
							while (reader.Read())
							{
								var imageID = reader.GetInt64(0);
								var tagID = reader.GetInt64(1);
								var image = _images.Find(i => i.ID == imageID);
								if (image != null)
								{
									var tag = _tags.Find(t => t.ID == tagID);
									if (tag != null)
									{
										image.Tags.Add(tag);
									}
								}
							}
						}
					}
					connection.Close();
				}
			}
			return _images;
		}

		public List<Tag> GetAllTags()
		{
			if (_tags == null)
			{
				_tags = new List<Tag>();
				using (var connection = new SQLiteConnection(_connectionString))
				{
					connection.Open();
					var text = "SELECT id, title FROM Tags;";
					using (var command = new SQLiteCommand(text, connection))
					{
						using (var reader = command.ExecuteReader())
						{
							while (reader.Read())
							{
								var tag = new Tag(reader.GetInt64(0), reader.GetString(1));
								_tags.Add(tag);
							}
						}
					}
					text = "SELECT parent_id, child_id FROM RelatedTags;";
					using (var command = new SQLiteCommand(text, connection))
					{
						using (var reader = command.ExecuteReader())
						{
							while (reader.Read())
							{
								var parentID = reader.GetInt64(0);
								var childID = reader.GetInt64(1);
								var parent = _tags.Find(t => t.ID == parentID);
								if (parent != null)
								{
									var child = _tags.Find(t => t.ID == childID);
									if (child != null)
									{
										parent.RelatedTags.Add(child);
									}
								}
							}
						}
					}
					connection.Close();
				}
			}
			return _tags;
		}

		public List<TaggedImage> GetImagesWithTags(List<Tag> tags)
		{
			GetAllImages();
			var result = _images.Where(image => tags.All(tag => image.Tags.Exists(t => t.ID == tag.ID)));
			return result.ToList();
		}

		public void UpdateImage(TaggedImage image)
		{
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				UpdateImageCommand(connection, image);
				connection.Close();
			}
			var oldTags = GetImageTags(image);
			List<Tag> added, deleted;
			Utility.CollectionUtil.CompareCollections(image.Tags, oldTags, out added, out deleted);
			UpdateImageTags(image, added, deleted);
		}

		public void UpdateImages(IEnumerable<TaggedImage> images)
		{
			if (images == null || images.Count() == 0)
			{
				return;
			}
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				try
				{
					foreach (var image in images)
					{
						UpdateImageCommand(connection, image);
						var oldTags = GetImageTags(image);
						List<Tag> added, deleted;
						Utility.CollectionUtil.CompareCollections(image.Tags, oldTags, out added, out deleted);
						UpdateImageTags(connection, image, added, deleted);
					}
					transaction.Commit();
				}
				catch (Exception ex)
				{
					transaction.Rollback();
				}
				connection.Close();
			}
		}

		public void UpdateImageTags(TaggedImage image, IEnumerable<Tag> added, IEnumerable<Tag> deleted)
		{
			var adding = added != null && added.Count() > 0;
			var deleting = deleted != null && deleted.Count() > 0;
			if (!adding && !deleting)
			{
				return;
			}
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				try
				{
					UpdateImageTags(connection, image, added, deleted);
					transaction.Commit();
				}
				catch (Exception)
				{
					transaction.Rollback();
				}
				connection.Close();
			}

			var empty = new Tag[0];
			foreach (var tag in added ?? empty)
			{
				if (!image.Tags.Contains(tag))
				{
					image.Tags.Add(tag);
				}
			}
			foreach (var tag in deleted ?? empty)
			{
				if (image.Tags.Contains(tag))
				{
					image.Tags.Remove(tag);
				}
			}
		}

		public void UpdateRelatedTags(Tag parent, IEnumerable<Tag> added, IEnumerable<Tag> deleted)
		{
			var adding = added != null && added.Count() > 0;
			var deleting = deleted != null && deleted.Count() > 0;
			if (!adding && !deleting)
			{
				return;
			}
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				try
				{
					UpdateRelatedTags(connection, parent, added, deleted);
					transaction.Commit();
				}
				catch (Exception)
				{
					transaction.Rollback();
				}
				connection.Close();
			}
		}

		public void UpdateTag(Tag tag)
		{
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				UpdateTagCommand(connection, tag);
				connection.Close();
			}
			var oldTags = GetRelatedTags(tag);
			List<Tag> added, deleted;
			Utility.CollectionUtil.CompareCollections(tag.RelatedTags, oldTags, out added, out deleted);
			UpdateRelatedTags(tag, added, deleted);
		}

		public void UpdateTags(IEnumerable<Tag> tags)
		{
			if (tags == null || tags.Count() == 0)
			{
				return;
			}
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var transaction = connection.BeginTransaction();
				try
				{
					foreach (var tag in tags)
					{
						UpdateTagCommand(connection, tag);
						var oldTags = GetRelatedTags(tag);
						List<Tag> added, deleted;
						Utility.CollectionUtil.CompareCollections(tag.RelatedTags, oldTags, out added, out deleted);
						UpdateRelatedTags(tag, added, deleted);
					}
					transaction.Commit();
				}
				catch (Exception ex)
				{
					transaction.Rollback();
				}
				connection.Close();
			}
		}

		private List<Tag> GetImageTags(TaggedImage image)
		{
			var tags = new List<Tag>();
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var text = "SELECT image_id, tag_id FROM TagsOfImages WHERE image_id = @i_id;";
				using (var command = new SQLiteCommand(text, connection))
				{
					AddParameter(command, "@i_id", System.Data.DbType.Int64, image.ID);
					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							var tagID = reader.GetInt64(1);
							var tag = _tags.Find(t => t.ID == tagID);
							if (tag != null)
							{
								tags.Add(tag);
							}
						}
					}
				}
				connection.Close();
			}
			return tags;
		}

		private List<Tag> GetRelatedTags(Tag parent)
		{
			var tags = new List<Tag>();
			using (var connection = new SQLiteConnection(_connectionString))
			{
				connection.Open();
				var text = "SELECT parent_id, child_id FROM RelatedTags WHERE parent_id = @p_id;";
				using (var command = new SQLiteCommand(text, connection))
				{
					AddParameter(command, "@p_id", System.Data.DbType.Int64, parent.ID);
					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							var tagID = reader.GetInt64(1);
							var tag = _tags.Find(t => t.ID == tagID);
							if (tag != null)
							{
								tags.Add(tag);
							}
						}
					}
				}
				connection.Close();
			}
			return tags;
		}

		#region SQL commands

		private static TaggedImage InsertImageCommand(SQLiteConnection connection, string imageFilePath, bool isFavourite)
		{
			var image = new TaggedImage(-1, imageFilePath, isFavourite);
			const string text = "INSERT INTO Images (is_favourite, filepath) VALUES(@is_fav, @path);";
			using (var command = new SQLiteCommand(text, connection))
			{
				AddParameter(command, "@is_fav", System.Data.DbType.Int64, image.IsFavourite ? LONG_TRUE : LONG_FALSE);
				AddParameter(command, "@path", System.Data.DbType.String, image.FilePath);
				command.ExecuteNonQuery(System.Data.CommandBehavior.KeyInfo);
				image.ID = connection.LastInsertRowId;
			}
			return image;
		}

		private static void InsertRelatedTagCommand(SQLiteConnection connection, Tag parent, Tag child)
		{
			const string text = "INSERT INTO RelatedTags (parent_id, child_id) VALUES(@p_id, @c_id);";
			using (var command = new SQLiteCommand(text, connection))
			{
				AddParameter(command, "@p_id", System.Data.DbType.Int64, parent.ID);
				AddParameter(command, "@c_id", System.Data.DbType.Int64, child.ID);
				command.ExecuteNonQuery(System.Data.CommandBehavior.KeyInfo);
			}
		}

		private static Tag InsertTagCommand(SQLiteConnection connection, string tagTitle)
		{
			var tag = new Tag(-1, tagTitle);
			const string text = "INSERT INTO Tags (title) VALUES(@title);";
			using (var command = new SQLiteCommand(text, connection))
			{
				AddParameter(command, "@title", System.Data.DbType.String, tag.Title);
				command.ExecuteNonQuery(System.Data.CommandBehavior.KeyInfo);
				tag.ID = connection.LastInsertRowId;
			}
			return tag;
		}

		private static void InsertTagOfImageCommand(SQLiteConnection connection, Tag tag, TaggedImage image)
		{
			const string text = "INSERT INTO TagsOfImages (image_id, tag_id) VALUES(@i_id, @t_id);";
			using (var command = new SQLiteCommand(text, connection))
			{
				AddParameter(command, "@i_id", System.Data.DbType.Int64, image.ID);
				AddParameter(command, "@t_id", System.Data.DbType.Int64, tag.ID);
				command.ExecuteNonQuery(System.Data.CommandBehavior.KeyInfo);
			}
		}

		private static void UpdateImageCommand(SQLiteConnection connection, TaggedImage image)
		{
			const string text = "UPDATE Images SET is_favourite = @is_fav, filepath = @path WHERE id = @id;";
			using (var command = new SQLiteCommand(text, connection))
			{
				AddParameter(command, "@id", System.Data.DbType.Int64, image.ID);
				AddParameter(command, "@is_fav", System.Data.DbType.Int64, image.IsFavourite ? LONG_TRUE : LONG_FALSE);
				AddParameter(command, "@path", System.Data.DbType.String, image.FilePath);
				command.ExecuteNonQuery();
			}
		}

		private static void UpdateTagCommand(SQLiteConnection connection, Tag tag)
		{
			const string text = "UPDATE Tags SET title = @title WHERE id = @id;";
			using (var command = new SQLiteCommand(text, connection))
			{
				AddParameter(command, "@id", System.Data.DbType.Int64, tag.ID);
				AddParameter(command, "@title", System.Data.DbType.String, tag.Title);
				command.ExecuteNonQuery();
			}
		}

		private static void DeleteImageCommand(SQLiteConnection connection, TaggedImage image)
		{
			const string text = "DELETE FROM Images WHERE id = @id;";
			using (var command = new SQLiteCommand(text, connection))
			{
				AddParameter(command, "@id", System.Data.DbType.Int64, image.ID);
				command.ExecuteNonQuery();
			}
		}

		private static void DeleteRelatedTagCommand(SQLiteConnection connection, Tag parent, Tag child)
		{
			const string text = "DELETE FROM RelatedTags WHERE parent_id = @p_id AND child_id = @c_id;";
			using (var command = new SQLiteCommand(text, connection))
			{
				AddParameter(command, "@p_id", System.Data.DbType.Int64, parent.ID);
				AddParameter(command, "@c_id", System.Data.DbType.Int64, child.ID);
				command.ExecuteNonQuery(System.Data.CommandBehavior.KeyInfo);
			}
		}

		private static void DeleteTagCommand(SQLiteConnection connection, Tag tag)
		{
			const string text = "DELETE FROM Tags WHERE id = @id;";
			using (var command = new SQLiteCommand(text, connection))
			{
				AddParameter(command, "@id", System.Data.DbType.Int64, tag.ID);
				command.ExecuteNonQuery();
			}
		}

		private static void DeleteTagOfImageCommand(SQLiteConnection connection, Tag tag, TaggedImage image)
		{
			const string text = "DELETE FROM TagsOfImages WHERE image_id = @i_id AND tag_id = @t_id;";
			using (var command = new SQLiteCommand(text, connection))
			{
				AddParameter(command, "@i_id", System.Data.DbType.Int64, image.ID);
				AddParameter(command, "@t_id", System.Data.DbType.Int64, tag.ID);
				command.ExecuteNonQuery();
			}
		}

		#endregion SQL commands

		private static void UpdateImageTags(SQLiteConnection connection, TaggedImage image, IEnumerable<Tag> added, IEnumerable<Tag> deleted)
		{
			var empty = new Tag[0];
			foreach (var tag in added ?? empty)
			{
				InsertTagOfImageCommand(connection, tag, image);
			}
			foreach (var tag in deleted ?? new Tag[0])
			{
				DeleteTagOfImageCommand(connection, tag, image);
			}
		}

		private static void UpdateRelatedTags(SQLiteConnection connection, Tag parent, IEnumerable<Tag> added, IEnumerable<Tag> deleted)
		{
			var empty = new Tag[0];
			foreach (var tag in added ?? empty)
			{
				InsertRelatedTagCommand(connection, parent, tag);
			}
			foreach (var tag in deleted ?? new Tag[0])
			{
				DeleteRelatedTagCommand(connection, parent, tag);
			}
		}

		private static void AddParameter(SQLiteCommand command, string paramName, System.Data.DbType dbType, object value)
		{
			var param = new SQLiteParameter(paramName, dbType);
			param.Value = value;
			command.Parameters.Add(param);
		}
	}
}
