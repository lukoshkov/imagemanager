﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ImageManager.Core;
using ImageManager.Data;
using System.Collections.ObjectModel;
using ImageManager.ViewModels;
using ImageManager.Utility;
using ImageManager.ViewModels.Comparers;

namespace ImageManager
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private static HashSet<string> IgnoredDirectories;
		private static HashSet<string> SupportedExtensions;

		private IStorage _storage;
		private ObservableCollection<TagViewModel> _tags;
		private List<TaggedImage> _images;
		private ImageViewModel _currentIVM;
		private ImageListViewModel _imagesVM;
		private ImageListViewModel _newImagesVM;
		private ImageListViewModel _lostImagesVM;
		private List<ImageListViewModel> _ILVMs;
		private TagViewModel _tagVM;
		private SettingsViewModel _settingsVM;
		private IComparer<Tag> _tagComparer;
		private IComparer<TagViewModel> _tagVMComparer;
		private IComparer<TaggedImage> _imageComparer;
		private IComparer<ImageViewModel> _imageVMComparer;
		private TagPickerViewModel _imageTPVM;
		private TagPickerViewModel _relatedTPVM;
		private TagPickerViewModel[] _TPVMs;
		private List<string> _filenames;

		static MainWindow()
		{
			DiskImageCache.Instance.DiskCachePath = "cache/images";
			IgnoredDirectories = new HashSet<string>();
			IgnoredDirectories.Add(System.IO.Path.GetFullPath(DiskImageCache.Instance.DiskCachePath));
			SupportedExtensions = new HashSet<string>() { ".png", ".jpg", ".jpeg", ".gif", ".bmp" };
		}

		public MainWindow()
		{
			InitializeComponent();
			Loaded += MainWindow_Loaded;
		}

		private void MainWindow_Loaded(object sender, RoutedEventArgs ev)
		{
			try
			{
				//_storage = new CsvStorage("tags.csv", "images.csv");
				_storage = new SQLiteStorage("Tagged.db");
				_tagComparer = new TagComparer();
				_imageComparer = new TaggedImageComparer();
				_tagVMComparer = new TagViewModelComparer(_tagComparer);
				_imageVMComparer = new ImageViewModelComparer();
				var tagList = _storage.GetAllTags();
				tagList.Sort(_tagComparer);
				_tags = new ObservableCollection<TagViewModel>(TagViewModel.Cast(tagList));
				_images = _storage.GetAllImages();
				_images.Sort(_imageComparer);
				var lost = _images.Where(IsImageLost).ToList();
				_imagesVM = new ImageListViewModel(_images, 1);
				_newImagesVM = new ImageListViewModel(new List<string>(), 1);
				_lostImagesVM = new ImageListViewModel(lost, 1);
				Images.SelectionChanged += (s, e) => SetCurrentIVM(_imagesVM.SelectedItem);
				NewImages.SelectionChanged += (s, e) => SetCurrentIVM(_newImagesVM.SelectedItem);
				LostImages.SelectionChanged += (s, e) => SetCurrentIVM(_lostImagesVM.SelectedItem);
				TaggedTab.DataContext = _imagesVM;
				NewTab.DataContext = _newImagesVM;
				LostTab.DataContext = _lostImagesVM;

				_imageTPVM = new TagPickerViewModel(_tags, AddTag);
				_imageTPVM.TagPicked += (s, e) =>
				{
					var picked = _imageTPVM.LastPickedTag;
					if (picked != null && _currentIVM != null)
					{
						foreach (var rt in picked.RelatedTags)
						{
							_imageTPVM.PickTag(rt);
						}
					}
				};
				TagPicker.DataContext = _imageTPVM;

				var searchTPVM = new TagPickerViewModel(_tags, AddTag);
				searchTPVM.TagPicked += (s, e) => SearchImages();
				searchTPVM.TagUnpicked += (s, e) => SearchImages();
				SearchTagPicker.DataContext = searchTPVM;
				SearchTagList.ItemsSource = searchTPVM.PickedTags;
				SearchTagList.SelectionChanged += (s, e) =>
				{
					var tag = SearchTagList.SelectedItem as TagViewModel;
					searchTPVM.UnpickTag(tag);
				};
				SearchButton.Click += (s, e) => SearchImages();
				
				_relatedTPVM = new TagPickerViewModel(_tags, AddTag);
				var managerTPVM = new TagPickerViewModel(_tags, AddTag);
				managerTPVM.PickedTags = null;
				managerTPVM.IsTagAutoDeselectEnabled = false;
				managerTPVM.TagPicked += (s, e) => SetCurrentTagVM(managerTPVM.LastPickedTag);
				ManagerTagPicker.DataContext = managerTPVM;
				RelatedTagsPicker.DataContext = _relatedTPVM;
				RelatedTagsList.SelectionChanged += (s, e) =>
				{
					var tag = RelatedTagsList.SelectedItem as TagViewModel;
					_relatedTPVM.UnpickTag(tag);
				};
				_TPVMs = new TagPickerViewModel[] { _imageTPVM, searchTPVM, _relatedTPVM, managerTPVM };

				var converter = FindResource("PathToThumbnail") as Converters.ImageConverter;
				_ILVMs = new List<ImageListViewModel>() { _imagesVM, _newImagesVM, _lostImagesVM };
				_settingsVM = new SettingsViewModel(converter, _ILVMs);
				_settingsVM.CanTrackDirectory = CanTrackDirectory;
				_settingsVM.TrackedDirs.CollectionChanged += (s, e) => LoadNewFiles(_settingsVM.TrackedDirs);
				SettingsTab.DataContext = _settingsVM;

				LoadNewFiles(_settingsVM.TrackedDirs);
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
				Close();
			}
		}

		private void ImageTagsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var list = sender as ListView;
			if (list != null)
			{
				var tag = list.SelectedItem as TagViewModel;
				if (tag != null)
				{
					_currentIVM.Tags.Remove(tag);
				}
			}
		}

		private void SaveImageChangesButton_Click(object sender, RoutedEventArgs e)
		{
			SaveImageChanges();
		}

		private void CancelImageChangesButton_Click(object sender, RoutedEventArgs e)
		{
			if (_currentIVM != null)
			{
				_currentIVM.CancelChanges();
				_imageTPVM.PickedTags = _currentIVM.Tags;
			}
		}

		private void CopyToClipboardButton_Click(object sender, RoutedEventArgs e)
		{
			if (_currentIVM != null)
			{
				try
				{
					FileUtil.ImageToClipboard(_currentIVM.FilePath);
				}
				catch (Exception)
				{ }
			}
		}

		private void ShowInExplorerButton_Click(object sender, RoutedEventArgs e)
		{
			if (_currentIVM != null)
			{
				try
				{
					FileUtil.ShowFileInExplorer(_currentIVM.FilePath);
				}
				catch (Exception)
				{ }
			}
		}

		private void MoveFileButton_Click(object sender, RoutedEventArgs e)
		{
			var ivm = _currentIVM;
			if (ivm != null)
			{
				var path = ivm.FilePath;
				var dialog = new Microsoft.Win32.SaveFileDialog();
				dialog.InitialDirectory = System.IO.Path.GetDirectoryName(path);
				dialog.FileName = System.IO.Path.GetFileName(path);
				dialog.DefaultExt = System.IO.Path.GetExtension(path);
				if (dialog.ShowDialog() == true && dialog.FileName != path)
				{
					System.IO.File.Move(path, dialog.FileName);
					ImageCache.Instance.Remove(path);
					DiskImageCache.Instance.Remove(path);
					ivm.FilePath = dialog.FileName;
					if (ivm.IsTagged)
					{
						SaveImageChanges();
					}
					else
					{
						ivm.ApplyChanges();
					}
				}
			}
		}

		private void LocateFileButton_Click(object sender, RoutedEventArgs e)
		{
			var ivm = _currentIVM;
			if (ivm != null)
			{
				var dialog = new Microsoft.Win32.OpenFileDialog();
				if (dialog.ShowDialog() == true)
				{
					ImageCache.Instance.Remove(ivm.FilePath);
					DiskImageCache.Instance.Remove(ivm.FilePath);
					ivm.FilePath = dialog.FileName;
					if (ivm.IsTagged)
					{
						SaveImageChanges();
						_lostImagesVM.RemoveImage(ivm);
						_imagesVM.Update();
					}
					else
					{
						ivm.ApplyChanges();
					}
				}
			}
		}

		private void DeleteFromTaggedButton_Click(object sender, RoutedEventArgs e)
		{
			var ivm = _currentIVM;
			if (ivm != null && ivm.IsTagged)
			{
				if (ivm.IsLost)
				{
					_lostImagesVM.RemoveImage(ivm);
				}
				_imagesVM.RemoveImage(ivm);
				_storage.DeleteImage(ivm.GetTaggedImage());
			}
		}

		private void TagUpdateButton_Click(object sender, RoutedEventArgs e)
		{
			UpdateTag(_tagVM);
		}

		private void TagDeleteButton_Click(object sender, RoutedEventArgs e)
		{
			if (_tagVM != null)
			{
				DeleteTag(_tagVM);
				SetCurrentTagVM(null);
			}
		}

		private void ClearCacheEntryButton_Click(object sender, RoutedEventArgs e)
		{
			var ivm = _currentIVM;
			if (ivm != null)
			{
				ImageCache.Instance.Remove(ivm.FilePath);
				DiskImageCache.Instance.Remove(ivm.FilePath);
				ivm.FilePath = ivm.FilePath;
			}
		}

		private void SetCurrentIVM(ImageViewModel ivm)
		{
			if (_currentIVM != null && _currentIVM.IsTagged && Properties.Settings.Default.IsAutoSaveEnabled)
			{
				SaveImageChanges();
			}
			_currentIVM = ivm;
			ImageView.DataContext = _currentIVM;
			_imageTPVM.PickedTags = _currentIVM?.Tags;
		}

		private void SetCurrentTagVM(TagViewModel tvm)
		{
			_tagVM = tvm;
			SelectedTag.DataContext = _tagVM;
			_relatedTPVM.PickedTags = _tagVM == null ? null : _tagVM.RelatedTags;
		}

		private void SaveImageChanges()
		{
			var ivm = _currentIVM;
			if (ivm != null)
			{
				if (ivm.IsTagged)
				{
					var changed = ivm.IsChanged;
					ivm.ApplyChanges();
					if (changed)
					{
						var image = ivm.GetTaggedImage();
						_storage.UpdateImage(image);
						_imagesVM.RemoveImage(ivm);
						_imagesVM.AddImageSorted(ivm, _imageVMComparer);
					}
				}
				else
				{
					var image = _storage.AddNewImage(ivm.FilePath, TagViewModel.Cast(ivm.Tags).ToList());
					_newImagesVM.RemoveImage(ivm);
					_imagesVM.AddImageSorted(new ImageViewModel(image), _imageVMComparer);
				}
			}
		}

		private TagViewModel AddTag(string title)
		{
			if (Core.Tag.IsTitleValid(title) && !IsTagExist(title))
			{
				var tag = TagViewModel.GetViewModel(_storage.AddNewTag(title));
				CollectionUtil.AddSorted(_tags, tag, _tagVMComparer);
				return tag;
			}
			return null;
		}

		private void UpdateTag(TagViewModel tagVM)
		{
			if (tagVM != null)
			{
				var relatedChanged = tagVM.AreRelatedTagsChanged;
				if (tagVM.IsTitleChanged || relatedChanged)
				{
					tagVM.ApplyChanges();
					_storage.UpdateTag(tagVM.Tag);
					_tags.Remove(tagVM);
					CollectionUtil.AddSorted(_tags, tagVM, _tagVMComparer);
				}
				if (relatedChanged)
				{
					var allIVMs = GetAllIVMs();
					var tVMsToAdd = tagVM.GetAllRelatedTags();
					var tagsToAdd = TagViewModel.Cast(tVMsToAdd);
					var updatedImages = new List<TaggedImage>();
					foreach (var image in _images.Where(i => i.Tags.Contains(tagVM.Tag)))
					{
						var changing = false;
						foreach (var rt in tagsToAdd)
						{
							if (!image.Tags.Contains(rt))
							{
								image.Tags.Add(rt);
								changing = true;
							}
						}
						if (changing)
						{
							updatedImages.Add(image);
						}
					}
					_storage.UpdateImages(updatedImages);
					foreach (var ivm in GetIVMsByImages(updatedImages).Where(i => i.Tags.Contains(tagVM)))
					{
						foreach (var rt in tVMsToAdd)
						{
							if (!ivm.Tags.Contains(rt))
							{
								ivm.Tags.Add(rt);
							}
						}
					}
				}
			}
		}

		private void DeleteTag(TagViewModel tagVM)
		{
			if (tagVM != null)
			{
				_storage.DeleteTag(tagVM.Tag);
				_tags.Remove(tagVM);
				foreach (var image in _images)
				{
					image.Tags.Remove(tagVM.Tag);
				}
				foreach (var ivm in GetAllIVMs())
				{
					ivm.Tags.Remove(tagVM);
				}
			}
		}

		private IEnumerable<ImageViewModel> GetIVMsByImages(IEnumerable<TaggedImage> images)
		{
			return GetAllIVMs().Where(ivm => images.Contains(ivm.GetTaggedImage()));
		}

		private IEnumerable<ImageViewModel> GetAllIVMs()
		{
			return _ILVMs.SelectMany(ilvm => ilvm.PageItems);
		}

		private bool IsTagExist(string tagTitle)
		{
			return ContainsTag(_tags, tagTitle);
		}

		private void SearchImages()
		{
			var items = SearchTagList.Items;
			List<Tag> tags = null;
			if (items != null && items.Count > 0)
			{
				tags = TagViewModel.Cast(items.Cast<TagViewModel>()).ToList();
			}
			var result = tags == null ? _images : _storage.GetImagesWithTags(tags);
			result.Sort(_imageComparer);
			_imagesVM.SetSource(result);
		}

		private void LoadNewFiles(ICollection<string> dirs)
		{
			var files = new List<string>();
			Func<string, bool> isNew = f => CanTrackFile(f) && !_images.Exists(image => image.FilePath == f);
			foreach (var dir in dirs)
			{
				files.AddRange(FileUtil.GetFilesFromDirectory(dir, isNew));
			}
			files.Sort();
			if (_filenames != null)
			{
				var deleted = CollectionUtil.GetFirstUniqueItems(_filenames, files).ToDictionary(f => f);
				var added = CollectionUtil.GetFirstUniqueItems(files, _filenames).Select(file => new ImageViewModel(file));
				_newImagesVM.UpdateImages(added, ivm => deleted.ContainsKey(ivm.FilePath), _imageVMComparer);
			}
			else
			{
				_newImagesVM.SetSource(files);
			}
			_filenames = files;
		}

		private static bool ContainsTag(IEnumerable<TagViewModel> tags, string tagTitle)
		{
			return tags.FirstOrDefault(t => t.Title == tagTitle) != null;
		}

		private static bool IsImageLost(TaggedImage image)
		{
			return !System.IO.File.Exists(image.FilePath);
		}

		private static bool CanTrackDirectory(string path)
		{
			return path != null && System.IO.Directory.Exists(path) 
				&& !IgnoredDirectories.Contains(System.IO.Path.GetFullPath(path));
		}

		private static bool CanTrackFile(string path)
		{
			return SupportedExtensions.Contains(System.IO.Path.GetExtension(path).ToLower());
		}
	}
}
