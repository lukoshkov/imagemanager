﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManager.Utility
{
	public static class CollectionUtil
	{
		public static int AddSorted<T>(Collection<T> collection, T item, IComparer<T> comparer)
		{
			var index = 0;
			while (index < collection.Count && comparer.Compare(collection[index], item) < 0)
			{
				index++;
			}
			collection.Insert(index, item);
			return index;
		}

		public static int AddSorted<T>(List<T> list, T item, IComparer<T> comparer)
		{
			var index = list.BinarySearch(item, comparer);
			if (index < 0)
			{
				index = ~index;
			}
			while (index < list.Count && comparer.Compare(list[index], item) < 0)
			{
				index++;
			}
			list.Insert(index, item);
			return index;
		}

		public static int MoveSorted<T>(List<T> list, int index, IComparer<T> comparer)
		{
			if (index < 0 || index >= list.Count)
			{
				throw new IndexOutOfRangeException();
			}
			var item = list[index];
			list.RemoveAt(index);
			return AddSorted(list, item, comparer);
		}

		public static void Move<T>(List<T> list, int oldIndex, int newIndex)
		{
			if (oldIndex < 0 || oldIndex >= list.Count || newIndex < 0 || newIndex >= list.Count)
			{
				throw new IndexOutOfRangeException();
			}
			var item = list[oldIndex];
			list.RemoveAt(oldIndex);
			list.Insert(newIndex, item);
		}

		public static void CompareCollections<T>(IEnumerable<T> first, IEnumerable<T> second, out List<T> firstUnique, out List<T> secondUnique)
		{
			firstUnique = GetFirstUniqueItems(first, second).ToList();
			secondUnique = GetFirstUniqueItems(second, first).ToList();
		}

		public static IEnumerable<T> GetFirstUniqueItems<T>(IEnumerable<T> first, IEnumerable<T> second)
		{
			return first.Where(item => !second.Contains(item));
		}
	}
}
