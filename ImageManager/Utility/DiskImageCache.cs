﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace ImageManager.Utility
{
	public class DiskImageCache
	{
		public static readonly DiskImageCache Instance;

		public string DiskCachePath { get; set; }

		static DiskImageCache()
		{
			Instance = new DiskImageCache();
		}

		private DiskImageCache()
		{ }

		public void Remove(string filePath)
		{
			if (Directory.Exists(DiskCachePath))
			{
				var filename = Path.GetFileName(filePath);
				var files = Directory.EnumerateFiles(DiskCachePath)
					.Where(f => Path.GetFileName(f).StartsWith(filename));
				foreach (var file in files)
				{
					File.Delete(file);
				}
			}
		}

		public void Clear()
		{
			if (Directory.Exists(DiskCachePath))
			{
				Directory.Delete(DiskCachePath, true);
			}
		}

		public bool TryLoadCached(string filePath, out BitmapImage bi, int decodeWidth, int decodeHeight)
		{
			var cached = GetDiskCacheFilePath(filePath, decodeWidth, decodeHeight);
			if (!TryLoadBitmap(cached, out bi, decodeWidth, decodeHeight))
			{
				if (!TryLoadBitmap(filePath, out bi, decodeWidth, decodeHeight))
				{
					return false;
				}
				TryWriteOnDisk(cached, bi);
			}
			return true;
		}

		public string GetDiskCacheFilePath(string filePath, int decodeWidth, int decodeHeight)
		{
			var filename = string.Format("{0}-{1}-{2}-{3}.png",
				Path.GetFileName(filePath), decodeWidth, decodeHeight, filePath.GetHashCode());
			return Path.GetFullPath(Path.Combine(DiskCachePath, filename));
		}

		public static bool TryLoadBitmap(string path, out BitmapImage bi, int decodeWidth, int decodeHeight)
		{
			try
			{
				bi = LoadBitmap(path, decodeWidth, decodeHeight);
				return true;
			}
			catch (Exception)
			{
				bi = null;
				return false;
			}
		}

		public static BitmapImage LoadBitmap(string path, int decodeWidth, int decodeHeight)
		{
			var uri = new Uri(path);
			var bi = new BitmapImage();
			bi.BeginInit();
			bi.CreateOptions = BitmapCreateOptions.IgnoreImageCache;
			bi.CacheOption = BitmapCacheOption.OnLoad;
			if (decodeWidth > 0)
			{
				bi.DecodePixelWidth = decodeWidth;
			}
			if (decodeHeight > 0)
			{
				bi.DecodePixelHeight = decodeHeight;
			}
			bi.UriSource = uri;
			bi.EndInit();
			return bi;
		}

		public static bool TryWriteOnDisk(string filePath, BitmapImage image)
		{
			try
			{
				WriteOnDisk(filePath, image);
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public static void WriteOnDisk(string filePath, BitmapImage image)
		{
			var dir = Path.GetDirectoryName(filePath);
			Directory.CreateDirectory(dir);
			if (image != null)
			{
				using (var stream = new FileStream(filePath, FileMode.Create))
				{
					var encoder = new PngBitmapEncoder();
					encoder.Frames.Add(BitmapFrame.Create(image));
					encoder.Save(stream);
				}
			}
		}
	}
}
