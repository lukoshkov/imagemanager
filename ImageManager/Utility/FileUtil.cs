﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManager.Utility
{
	public static class FileUtil
	{
		public static IEnumerable<string> GetFilesFromDirectory(string dirPath, Func<string, bool> filter)
		{
			return Directory.EnumerateFiles(dirPath)
				.Where(f => filter(f))
				.Select(f => Path.GetFullPath(f));
		}

		public static void ShowFileInExplorer(string path)
		{
			if (File.Exists(path))
			{
				System.Diagnostics.Process.Start("explorer.exe", string.Format("/select, \"{0}\"", path));
			}
		}

		public static void ImageToClipboard(string filePath)
		{
			var uri = new Uri(filePath);
			var image = new System.Windows.Media.Imaging.BitmapImage(uri);
			System.Windows.Clipboard.SetImage(image);
		}
	}
}
