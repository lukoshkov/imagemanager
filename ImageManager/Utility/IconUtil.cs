﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ImageManager.Utility
{
	public static class IconUtil
	{
		public enum IconSize
		{
			Small = 16,
			Large = 32,
			ExtraLarge = 48,
			Jumbo = 256
		}

		private static BitmapSource _folderIconSmall;
		private static BitmapSource _folderIconLarge;

		public static BitmapSource FolderSmall
		{
			get { return _folderIconSmall ?? (_folderIconSmall = GetFolderIcon(IconSize.Small)); }
		}

		public static BitmapSource FolderLarge
		{
			get { return _folderIconLarge ?? (_folderIconLarge = GetFolderIcon(IconSize.Large)); }
		}

		public static BitmapSource GetFileIconBitmap(string filePath, IconSize size)
		{
			switch (size)
			{
				case IconSize.Small:
					return ExtractFromPath(filePath, SHGFI.SmallIcon);
				case IconSize.Large:
					return ExtractFromPath(filePath, SHGFI.LargeIcon);
				case IconSize.ExtraLarge:
				case IconSize.Jumbo:
					return ExtractLargeFromPath(filePath, size);
			}
			return null;
		}

		#region Private methods

		private static BitmapSource GetFolderIcon(IconSize size)
		{
			var tmpDir = Directory.CreateDirectory(Path.Combine(Path.GetTempPath(), Guid.NewGuid().ToString())).FullName;
			var icon = GetFileIconBitmap(tmpDir, size);
			Directory.Delete(tmpDir);
			return icon;
		}

		private static BitmapSource BitmapFromHIcon(IntPtr hIcon)
		{
			return System.Windows.Interop.Imaging.CreateBitmapSourceFromHIcon(hIcon, Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
		}

		private static BitmapSource ExtractLargeFromPath(string path, IconSize size)
		{
			var hIcon = GetJumboIcon(GetIconIndex(path), size);
			var bmp = BitmapFromHIcon(hIcon);
			bmp.DownloadCompleted += (s, e) => DestroyIcon(hIcon);
			return bmp;
		}

		private static BitmapSource ExtractFromPath(string path, SHGFI size)
		{
			SHFILEINFO shinfo = new SHFILEINFO();
			SHGetFileInfo(
				path,
				0, ref shinfo, (uint)Marshal.SizeOf(shinfo),
				(uint)(SHGFI.Icon | size));
			var bmp = BitmapFromHIcon(shinfo.hIcon);
			bmp.DownloadCompleted += (s, e) => DestroyIcon(shinfo.hIcon);
			//bmp.Freeze();
			return bmp;
		}

		private static int GetIconIndex(string pszFile)
		{
			SHFILEINFO sfi = new SHFILEINFO();
			SHGetFileInfo(pszFile, 0, ref sfi, (uint)Marshal.SizeOf(sfi), 
				(uint)(SHGFI.SysIconIndex | SHGFI.LargeIcon | SHGFI.UseFileAttributes));
			return sfi.iIcon;
		}
		
		private static IntPtr GetJumboIcon(int iImage, IconSize size)
		{
			int iImageList = SHIL_JUMBO;
			switch (size)
			{
				case IconSize.Small:
					iImageList = SHIL_SMALL;
					break;
				case IconSize.Large:
					iImageList = SHIL_LARGE;
					break;
				case IconSize.ExtraLarge:
					iImageList = SHIL_EXTRALARGE;
					break;
			}
			IImageList spiml = null;
			Guid guil = new Guid(IID_IImageList2); //or IID_IImageList

			SHGetImageList(iImageList, ref guil, ref spiml);
			IntPtr hIcon = IntPtr.Zero;
			spiml.GetIcon(iImage, ILD_TRANSPARENT | ILD_IMAGE, ref hIcon);

			return hIcon;
		}

		#endregion

		#region Win32

		private const string IID_IImageList = "46EB5926-582E-4017-9FDF-E8998DAA0950";
		private const string IID_IImageList2 = "192B9D83-50FC-457B-90A0-2B82A8B5DAE1";

		private const int SHIL_LARGE = 0x0;
		private const int SHIL_SMALL = 0x1;
		private const int SHIL_EXTRALARGE = 0x2;
		private const int SHIL_SYSSMALL = 0x3;
		private const int SHIL_JUMBO = 0x4;
		private const int SHIL_LAST = 0x4;

		public const int ILD_TRANSPARENT = 0x00000001;
		public const int ILD_IMAGE = 0x00000020;

		[DllImport("shell32.dll")]
		private static extern IntPtr SHGetFileInfo(string pszPath, uint dwFileAttributes, ref SHFILEINFO psfi, uint cbSizeFileInfo, uint uFlags);

		[DllImport("shell32.dll")]
		private static extern bool DestroyIcon(IntPtr hIcon);

		[DllImport("shell32.dll", EntryPoint = "#727")]
		private extern static int SHGetImageList(int iImageList, ref Guid riid, ref IImageList ppv);

		//[DllImport("user32.dll", EntryPoint = "DestroyIcon", SetLastError = true)]
		//private static unsafe extern int DestroyIcon(IntPtr hIcon);

		[DllImport("shell32.dll")]
		private static extern uint SHGetIDListFromObject([MarshalAs(UnmanagedType.IUnknown)] object iUnknown, out IntPtr ppidl);

		#region Structures

		[Flags]
		enum SHGFI : uint
		{
			/// <summary>get icon</summary>
			Icon = 0x000000100,
			/// <summary>get display name</summary>
			DisplayName = 0x000000200,
			/// <summary>get type name</summary>
			TypeName = 0x000000400,
			/// <summary>get attributes</summary>
			Attributes = 0x000000800,
			/// <summary>get icon location</summary>
			IconLocation = 0x000001000,
			/// <summary>return exe type</summary>
			ExeType = 0x000002000,
			/// <summary>get system icon index</summary>
			SysIconIndex = 0x000004000,
			/// <summary>put a link overlay on icon</summary>
			LinkOverlay = 0x000008000,
			/// <summary>show icon in selected state</summary>
			Selected = 0x000010000,
			/// <summary>get only specified attributes</summary>
			Attr_Specified = 0x000020000,
			/// <summary>get large icon</summary>
			LargeIcon = 0x000000000,
			/// <summary>get small icon</summary>
			SmallIcon = 0x000000001,
			/// <summary>get open icon</summary>
			OpenIcon = 0x000000002,
			/// <summary>get shell size icon</summary>
			ShellIconSize = 0x000000004,
			/// <summary>pszPath is a pidl</summary>
			PIDL = 0x000000008,
			/// <summary>use passed dwFileAttribute</summary>
			UseFileAttributes = 0x000000010,
			/// <summary>apply the appropriate overlays</summary>
			AddOverlays = 0x000000020,
			/// <summary>Get the index of the overlay in the upper 8 bits of the iIcon</summary>
			OverlayIndex = 0x000000040,
		}

		[StructLayout(LayoutKind.Sequential)]
		private struct SHFILEINFO
		{
			public const int NAMESIZE = 80;
			public IntPtr hIcon;
			public int iIcon;
			public uint dwAttributes;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 260)]
			public string szDisplayName;
			[MarshalAs(UnmanagedType.ByValTStr, SizeConst = 80)]
			public string szTypeName;
		};


		[StructLayout(LayoutKind.Sequential)]
		private struct RECT
		{
			public int left, top, right, bottom;
		}

		[StructLayout(LayoutKind.Sequential)]
		private struct POINT
		{
			int x;
			int y;
		}

		[StructLayout(LayoutKind.Sequential)]
		private struct IMAGELISTDRAWPARAMS
		{
			public int cbSize;
			public IntPtr himl;
			public int i;
			public IntPtr hdcDst;
			public int x;
			public int y;
			public int cx;
			public int cy;
			public int xBitmap;    // x offest from the upperleft of bitmap
			public int yBitmap;    // y offset from the upperleft of bitmap
			public int rgbBk;
			public int rgbFg;
			public int fStyle;
			public int dwRop;
			public int fState;
			public int Frame;
			public int crEffect;
		}

		[StructLayout(LayoutKind.Sequential)]
		private struct IMAGEINFO
		{
			public IntPtr hbmImage;
			public IntPtr hbmMask;
			public int Unused1;
			public int Unused2;
			public RECT rcImage;
		}

		[ComImportAttribute()]
		[GuidAttribute("46EB5926-582E-4017-9FDF-E8998DAA0950")]
		[InterfaceTypeAttribute(ComInterfaceType.InterfaceIsIUnknown)]
		private interface IImageList
		{
			[PreserveSig]
			int Add(
			IntPtr hbmImage,
			IntPtr hbmMask,
			ref int pi);

			[PreserveSig]
			int ReplaceIcon(
			int i,
			IntPtr hicon,
			ref int pi);

			[PreserveSig]
			int SetOverlayImage(
			int iImage,
			int iOverlay);

			[PreserveSig]
			int Replace(
			int i,
			IntPtr hbmImage,
			IntPtr hbmMask);

			[PreserveSig]
			int AddMasked(
			IntPtr hbmImage,
			int crMask,
			ref int pi);

			[PreserveSig]
			int Draw(
			ref IMAGELISTDRAWPARAMS pimldp);

			[PreserveSig]
			int Remove(
		int i);

			[PreserveSig]
			int GetIcon(
			int i,
			int flags,
			ref IntPtr picon);

			[PreserveSig]
			int GetImageInfo(
			int i,
			ref IMAGEINFO pImageInfo);

			[PreserveSig]
			int Copy(
			int iDst,
			IImageList punkSrc,
			int iSrc,
			int uFlags);

			[PreserveSig]
			int Merge(
			int i1,
			IImageList punk2,
			int i2,
			int dx,
			int dy,
			ref Guid riid,
			ref IntPtr ppv);

			[PreserveSig]
			int Clone(
			ref Guid riid,
			ref IntPtr ppv);

			[PreserveSig]
			int GetImageRect(
			int i,
			ref RECT prc);

			[PreserveSig]
			int GetIconSize(
			ref int cx,
			ref int cy);

			[PreserveSig]
			int SetIconSize(
			int cx,
			int cy);

			[PreserveSig]
			int GetImageCount(
		ref int pi);

			[PreserveSig]
			int SetImageCount(
			int uNewCount);

			[PreserveSig]
			int SetBkColor(
			int clrBk,
			ref int pclr);

			[PreserveSig]
			int GetBkColor(
			ref int pclr);

			[PreserveSig]
			int BeginDrag(
			int iTrack,
			int dxHotspot,
			int dyHotspot);

			[PreserveSig]
			int EndDrag();

			[PreserveSig]
			int DragEnter(
			IntPtr hwndLock,
			int x,
			int y);

			[PreserveSig]
			int DragLeave(
			IntPtr hwndLock);

			[PreserveSig]
			int DragMove(
			int x,
			int y);

			[PreserveSig]
			int SetDragCursorImage(
			ref IImageList punk,
			int iDrag,
			int dxHotspot,
			int dyHotspot);

			[PreserveSig]
			int DragShowNolock(
			int fShow);

			[PreserveSig]
			int GetDragImage(
			ref POINT ppt,
			ref POINT pptHotspot,
			ref Guid riid,
			ref IntPtr ppv);

			[PreserveSig]
			int GetItemFlags(
			int i,
			ref int dwFlags);

			[PreserveSig]
			int GetOverlayImage(
			int iOverlay,
			ref int piIndex);
		};

		#endregion

		#endregion
	}
}
