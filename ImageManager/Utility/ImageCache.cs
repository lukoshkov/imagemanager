﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace ImageManager.Utility
{
	public class ImageCache
	{
		struct ImageCacheKey
		{
			public int Width;
			public int Height;
			public string Path;

			public ImageCacheKey(int width, int height, string path)
			{
				Width = width;
				Height = height;
				Path = path;
			}

			public override int GetHashCode()
			{
				unchecked
				{
					int hash = 17;
					hash = 23 * hash + Width.GetHashCode();
					hash = 23 * hash + Height.GetHashCode();
					hash = 23 * hash + Path == null ? 0 : Path.GetHashCode();
					return hash;
				}
			}

			public override bool Equals(object obj)
			{
				if (obj is ImageCacheKey)
				{
					var other = (ImageCacheKey)obj;
					return Width == other.Width
						&& Height == other.Height
						&& string.Equals(Path, other.Path);
				}
				return false;
			}
		}
		

		private ConcurrentDictionary<ImageCacheKey, ImageSource> _cache;
		public static readonly ImageCache Instance;

		static ImageCache()
		{
			Instance = new ImageCache();
		}

		public ImageCache()
		{
			_cache = new ConcurrentDictionary<ImageCacheKey, ImageSource>();
		}

		public bool TryGetValue(int width, int height, string path, out ImageSource image)
		{
			return _cache.TryGetValue(new ImageCacheKey(width, height, path), out image);
		}

		public bool TryAdd(int width, int height, string path, ImageSource image)
		{
			return _cache.TryAdd(new ImageCacheKey(width, height, path), image);
		}

		public void Remove(string path)
		{
			ImageSource image;
			var keys = _cache.Keys.Where(key => string.Equals(key.Path, path));
			foreach (var key in keys)
			{
				_cache.TryRemove(key, out image);
			}
		}

		public void Remove(int width, int height)
		{
			ImageSource image;
			var keys = _cache.Keys.Where(key => key.Width == width && key.Height == height);
			foreach (var key in keys)
			{
				_cache.TryRemove(key, out image);
			}
		}

		public void Remove(int width, int height, string path)
		{
			ImageSource image;
			_cache.TryRemove(new ImageCacheKey(width, height, path), out image);
		}

		public void Clear()
		{
			_cache.Clear();
		}
	}
}
