﻿using ImageManager.Core;
using System;
using System.Collections.Generic;

namespace ImageManager.ViewModels.Comparers
{
	public class ImageViewModelComparer : IComparer<ImageViewModel>
	{
		public int Compare(ImageViewModel x, ImageViewModel y)
		{
			if (x == null && y == null) return 0;
			if (x == null) return -1;
			if (y == null) return 1;
			var score = (y.IsFavourite ? 1 : 0) - (x.IsFavourite ? 1 : 0);
			if (score != 0)
			{
				return score;
			}
			return x.FilePath.CompareTo(y.FilePath);
		}
	}
}
