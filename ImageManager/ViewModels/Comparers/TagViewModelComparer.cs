﻿using ImageManager.Core;
using System;
using System.Collections.Generic;

namespace ImageManager.ViewModels.Comparers
{
	public class TagViewModelComparer : IComparer<TagViewModel>
	{
		private IComparer<Tag> _comparer;

		public TagViewModelComparer(IComparer<Tag> tagComparer)
		{
			_comparer = tagComparer;
		}

		public int Compare(TagViewModel x, TagViewModel y)
		{
			if (x == null && y == null) return 0;
			if (x == null) return -1;
			if (y == null) return 1;
			return _comparer?.Compare(x.Tag, y.Tag) ?? 0;
		}
	}
}
