﻿using ImageManager.Commands;
using ImageManager.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ImageManager.ViewModels
{
	public class ImageListViewModel : INotifyPropertyChanged
	{
		private bool _isPageSizeLimited;
		private int _perPage = 10;
		private int _pageIndex;
		private ObservableCollection<ImageViewModel> _pageItems;
		private ImageViewModel _selectedItem;
		private List<ImageViewModel> _source;

		#region Properties

		public bool IsPageSizeLimited
		{
			get
			{
				return _isPageSizeLimited;
			}
			set
			{
				_isPageSizeLimited = value;
				NotifyPropertyChanged(nameof(IsPageSizeLimited));
				NotifyPageSizeChanged();
				Update();
			}
		}

		public int ImagesPerPage
		{
			get
			{
				return IsPageSizeLimited ? _perPage : ItemsCount;
			}
			set
			{
				if (value > 0)
				{
					_perPage = value;
					NotifyPropertyChanged(nameof(ImagesPerPage));
					NotifyPageSizeChanged();
					Update();
				}
			}
		}

		public int PageIndex
		{
			get
			{
				return _pageIndex;
			}
			set
			{
				var page = GetPage(value);
				if (page != null)
				{
					_pageIndex = value;
					NotifyPropertyChanged(nameof(PageIndex));
					PageItems = page;
				}
			}
		}

		public int PagesCount
		{
			get
			{
				return (int)Math.Ceiling(ItemsCount / Math.Max(ImagesPerPage, 1.0));
			}
		}

		public int ItemsCount
		{
			get
			{
				if (_source != null)
				{
					return _source.Count;
				}
				return 0;
			}
		}

		public bool IsEmpty
		{
			get
			{
				return ItemsCount == 0;
			}
		}

		public ObservableCollection<ImageViewModel> PageItems
		{
			get
			{
				return _pageItems;
			}
			private set
			{
				_pageItems = value;
				NotifyPropertyChanged(nameof(PageItems));
			}
		}

		public ImageViewModel SelectedItem
		{
			get
			{
				return _selectedItem;
			}
			set
			{
				_selectedItem = value;
				NotifyPropertyChanged(nameof(SelectedItem));
			}
		}

		#endregion

		#region Commands

		private ICommand _nextPageCommand;
		private ICommand _lastPageCommand;
		private ICommand _prevPageCommand;
		private ICommand _firstPageCommand;

		public ICommand GoNextPageCommand
		{
			get { return _nextPageCommand ?? (_nextPageCommand = new RelayCommand(_ => PageIndex++, _ => PageIndex < PagesCount - 1)); }
		}

		public ICommand GoLastPageCommand
		{
			get { return _lastPageCommand ?? (_lastPageCommand = new RelayCommand(_ => PageIndex = PagesCount - 1, _ => PageIndex < PagesCount - 1)); }
		}

		public ICommand GoPrevPageCommand
		{
			get { return _prevPageCommand ?? (_prevPageCommand = new RelayCommand(_ => PageIndex--, _ => PageIndex > 0)); }
		}

		public ICommand GoFirstPageCommand
		{
			get { return _firstPageCommand ?? (_firstPageCommand = new RelayCommand(_ => PageIndex = 0, _ => PageIndex > 0)); }
		}

		#endregion

		public ImageListViewModel()
		{
		}

		public ImageListViewModel(List<string> fileNames, int imagesPerPage)
		{
			ImagesPerPage = imagesPerPage;
			SetSource(fileNames);
		}

		public ImageListViewModel(List<TaggedImage> images, int imagesPerPage)
		{
			ImagesPerPage = imagesPerPage;
			SetSource(images);
		}

		public ImageListViewModel(List<ImageViewModel> images, int imagesPerPage)
		{
			ImagesPerPage = imagesPerPage;
			SetSource(images);
		}

		public void SetSource(List<TaggedImage> images)
		{
			SetSource(images.Select(i => new ImageViewModel(i)).ToList());
		}

		public void SetSource(List<string> fileNames)
		{
			SetSource(fileNames.Select(f => new ImageViewModel(f)).ToList());
		}

		public void SetSource(List<ImageViewModel> imageVMs)
		{
			_source = imageVMs;
			NotifySourceChanged();
			PageIndex = 0;
		}

		public void Update()
		{
			PageIndex = Math.Min(PageIndex, Math.Max(PagesCount - 1, 0));
		}

		public void RemoveSelected()
		{
			RemoveImage(SelectedItem);
		}

		public bool CanSetPage(int index)
		{
			return index >= 0 && index < PagesCount;
		}

		public int AddImage(ImageViewModel ivm)
		{
			return AddImageSorted(ivm, null);
		}

		public int AddImageSorted(ImageViewModel ivm, IComparer<ImageViewModel> comparer)
		{
			var index = _source.Count;
			if (comparer != null)
			{
				index = Utility.CollectionUtil.AddSorted(_source, ivm, comparer);
			}
			else
			{
				_source.Add(ivm);
			}
			var pageStart = GetPageStartIndex(PageIndex);
			var pageSize = GetPageItemsCount(pageStart);
			var indexInPage = index - pageStart;
			if (indexInPage >= 0 && (!IsPageSizeLimited || indexInPage < _perPage))
			{
				PageItems.Insert(indexInPage, ivm);
				if (IsPageSizeLimited && PageItems.Count > _perPage)
				{
					PageItems.RemoveAt(PageItems.Count - 1);
				}
			}
			NotifySourceChanged();
			return index;
		}

		public void RemoveImage(Predicate<ImageViewModel> match)
		{
			RemoveImage(_source.Find(match));
		}

		public void RemoveImage(ImageViewModel ivm)
		{
			var index = _source.IndexOf(ivm);
			if (index == -1)
			{
				return;
			}
			_source.RemoveAt(index);
			var pageStart = GetPageStartIndex(PageIndex);
			var pageSize = GetPageItemsCount(pageStart);
			var indexInPage = index - pageStart;
			if (indexInPage >= 0 && indexInPage < PageItems.Count)
			{
				PageItems.RemoveAt(indexInPage);
				var newIndex = pageStart + PageItems.Count;
				if (newIndex < _source.Count)
				{
					PageItems.Add(_source[newIndex]);
				}
			}
			NotifySourceChanged();
		}

		public void UpdateImages(IEnumerable<ImageViewModel> itemsToAdd, Predicate<ImageViewModel> removeMatch, IComparer<ImageViewModel> comparer)
		{
			_source.RemoveAll(removeMatch);
			_source.AddRange(itemsToAdd);
			NotifySourceChanged();
			Sort(comparer);
		}

		public void Sort(IComparer<ImageViewModel> comparer)
		{
			if (comparer != null)
			{
				_source.Sort(comparer);
				Update();
			}
		}

		#region Private methods

		private ObservableCollection<ImageViewModel> GetPage(int index)
		{
			if (_source == null || index < 0 || index >= PagesCount && index != 0)
			{
				return null;
			}
			var startIndex = GetPageStartIndex(index);
			var count = GetPageItemsCount(startIndex);
			return new ObservableCollection<ImageViewModel>(_source.GetRange(startIndex, count));
		}

		private int GetPageStartIndex(int pageIndex)
		{
			return pageIndex * ImagesPerPage;
		}

		private int GetPageItemsCount(int pageStartIndex)
		{
			return Math.Min(ImagesPerPage, _source.Count - pageStartIndex);
		}

		private void NotifySourceChanged()
		{
			NotifyPropertyChanged(nameof(ItemsCount));
			NotifyPropertyChanged(nameof(IsEmpty));
			NotifyPageSizeChanged();
		}

		private void NotifyPageSizeChanged()
		{
			NotifyPropertyChanged(nameof(PagesCount));
		}

		#endregion

		#region INotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected void NotifyPropertyChanged(string propertyName)
		{
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion
	}
}
