﻿using ImageManager.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManager.ViewModels
{
	public class ImageViewModel : INotifyPropertyChanged
	{
		private TaggedImage _taggedImage;
		private string _filePath;
		private string _originalFilePath;
		private bool _isFavourite;
		private ObservableCollection<TagViewModel> _tags;
		

		public string FilePath
		{
			get
			{
				return _filePath;
			}
			set
			{
				_filePath = value;
				NotifyPropertyChanged(nameof(FilePath));
				NotifyPropertyChanged(nameof(FileName));
				NotifyPropertyChanged(nameof(IsLost));
			}
		}

		public string FileName
		{
			get
			{
				var path = FilePath;
				if (path != null)
				{
					return System.IO.Path.GetFileName(path);
				}
				return "";
			}
			set
			{
				var path = FilePath;
				if (path != null)
				{
					var dir = System.IO.Path.GetDirectoryName(path);
					var ext = System.IO.Path.GetExtension(path);
					FilePath = System.IO.Path.ChangeExtension(System.IO.Path.Combine(dir, value), ext);
				}
			}
		}

		public bool IsFavourite
		{
			get
			{
				return IsTagged && _isFavourite;
			}
			set
			{
				if (IsTagged)
				{
					_isFavourite = value;
					NotifyPropertyChanged(nameof(IsFavourite));
					NotifyPropertyChanged(nameof(IsChanged));
				}
			}
		}

		public bool IsTagged
		{
			get
			{
				return _taggedImage != null;
			}
		}

		public bool IsChanged
		{
			get
			{
				if (IsTagged)
				{
					return FilePath != _taggedImage.FilePath || _isFavourite != _taggedImage.IsFavourite || AreTagsChanged();
				}
				else
				{
					return FilePath != _originalFilePath;
				}
			}
		}

		public bool IsLost
		{
			get
			{
				return !System.IO.File.Exists(FilePath);
			}
		}

		public int TagsCount
		{
			get
			{
				return _taggedImage == null ? 0 : _taggedImage.Tags.Count;
			}
		}

		public ObservableCollection<TagViewModel> Tags
		{
			get
			{
				return _tags;
			}
			set
			{
				_tags = value;
				NotifyPropertyChanged(nameof(Tags));
			}
		}


		public ImageViewModel()
		{
		}

		public ImageViewModel(string filePath)
		{
			Update(filePath);
		}

		public ImageViewModel(TaggedImage image)
		{
			Update(image);
		}

		public void Update(string filePath)
		{
			_taggedImage = null;
			_isFavourite = false;
			_originalFilePath = filePath;
			FilePath = filePath;
			Tags = new ObservableCollection<TagViewModel>();
			NotifyUpdated();
		}

		public void Update(TaggedImage image)
		{
			_taggedImage = image;
			_isFavourite = _taggedImage.IsFavourite;
			FilePath = image.FilePath;
			Tags = new ObservableCollection<TagViewModel>(TagViewModel.Cast(image.Tags));
			NotifyUpdated();
		}

		public TaggedImage GetTaggedImage()
		{
			return _taggedImage;
		}

		public void ApplyChanges()
		{
			if (IsTagged)
			{
				_taggedImage.FilePath = FilePath;
				_taggedImage.IsFavourite = IsFavourite;
				_taggedImage.Tags.Clear();
				_taggedImage.Tags.AddRange(TagViewModel.Cast(Tags));
				NotifyPropertyChanged(nameof(IsChanged));
				NotifyPropertyChanged(nameof(TagsCount));
			}
			_originalFilePath = _filePath;
		}

		public void CancelChanges()
		{
			if (IsTagged)
			{
				Update(_taggedImage);
			}
			else
			{
				Update(_originalFilePath);
			}
		}

		private void NotifyUpdated()
		{
			NotifyPropertyChanged(nameof(IsTagged));
			NotifyPropertyChanged(nameof(IsChanged));
			NotifyPropertyChanged(nameof(TagsCount));
			NotifyPropertyChanged(nameof(IsFavourite));
		}

		private bool AreTagsChanged()
		{
			if (Tags.Count != _taggedImage.Tags.Count)
			{
				return true;
			}
			List<Tag> added, deleted;
			CompareTagCollections(out added, out deleted);
			return added.Count > 0 || deleted.Count > 0;
		}

		private void CompareTagCollections(out List<Tag> added, out List<Tag> deleted)
		{
			Utility.CollectionUtil.CompareCollections(TagViewModel.Cast(Tags), _taggedImage.Tags, out added, out deleted);
		}

		#region INotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected void NotifyPropertyChanged(string propertyName)
		{
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion
	}
}
