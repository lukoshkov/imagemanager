﻿using ImageManager.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ImageManager.ViewModels
{
	public class SettingsViewModel : INotifyPropertyChanged
	{
		#region Fields

		private int[] _perPageValues;
		private ObservableCollection<string> _trackedDirs;
		private string _selectedDir;
		private string _selectedTreeDir;
		private int _perPageIndex;

		private Properties.Settings _settings;
		private List<ImageListViewModel> _imagesVMs;
		private Converters.ImageConverter _converter;

		private ICommand _clearCacheCommand;
		private ICommand _addDirCommand;
		private ICommand _removeDirCommand;

		public Predicate<string> CanTrackDirectory;

		#endregion Fields

		#region Properties

		public bool IsRamCacheEnabled
		{
			get
			{
				return _settings.IsRamCacheEnabled;
			}
			set
			{
				if (_settings.IsRamCacheEnabled != value)
				{
					_settings.IsRamCacheEnabled = value;
					_settings.Save();
				}
				if (_converter != null)
				{
					_converter.IsCached = value;
				}
				NotifyPropertyChanged(nameof(IsRamCacheEnabled));
			}
		}

		public bool IsDiskCacheEnabled
		{
			get
			{
				return _settings.IsDiskCacheEnabled;
			}
			set
			{
				if (_settings.IsDiskCacheEnabled != value)
				{
					_settings.IsDiskCacheEnabled = value;
					_settings.Save();
				}
				if (_converter != null)
				{
					_converter.IsDiskCacheEnabled = value;
				}
				NotifyPropertyChanged(nameof(IsDiskCacheEnabled));
			}
		}

		public bool IsAutoSaveEnabled
		{
			get
			{
				return _settings.IsAutoSaveEnabled;
			}
			set
			{
				if (_settings.IsAutoSaveEnabled != value)
				{
					_settings.IsAutoSaveEnabled = value;
					_settings.Save();
				}
				NotifyPropertyChanged(nameof(IsAutoSaveEnabled));
			}
		}

		public bool IsPageSizeLimited
		{
			get
			{
				return _settings.IsPageSizeLimited;
			}
			set
			{
				if (_settings.IsPageSizeLimited != value)
				{
					_settings.IsPageSizeLimited = value;
					_settings.Save();
				}
				foreach (var vm in _imagesVMs)
				{
					vm.IsPageSizeLimited = value;
				}
				NotifyPropertyChanged(nameof(IsPageSizeLimited));
			}
		}

		public ObservableCollection<string> TrackedDirs
		{
			get
			{
				return _trackedDirs;
			}
			set
			{
				_trackedDirs = value;
				NotifyPropertyChanged(nameof(TrackedDirs));
			}
		}

		public int[] PerPageValues
		{
			get
			{
				if (_perPageValues == null)
				{
					_perPageValues = new int[] { 50, 100, 200, 500, 1000, 2000 };
				}
				return _perPageValues;
			}
		}

		public int PerPageIndex
		{
			get
			{
				return _perPageIndex;
			}
			set
			{
				_perPageIndex = Math.Min(Math.Max(value, 0), PerPageValues.Length);
				var perPage = ImagesPerPage;
				if (_settings.ImagesPerPage != perPage)
				{
					_settings.ImagesPerPage = perPage;
					_settings.Save();
				}
				foreach (var vm in _imagesVMs)
				{
					vm.ImagesPerPage = perPage;
				}
				NotifyPropertyChanged(nameof(PerPageIndex));
				NotifyPropertyChanged(nameof(ImagesPerPage));
			}
		}

		public int ImagesPerPage
		{
			get
			{
				return PerPageValues[PerPageIndex];
			}
			set
			{
				var index = 0;
				var lastIndex = PerPageValues.Length - 1;
				while (index < lastIndex && value > PerPageValues[index])
				{
					index++;
				}
				PerPageIndex = index;
			}
		}

		public ICommand ClearCacheCommand
		{
			get
			{
				if (_clearCacheCommand == null)
				{
					_clearCacheCommand = new RelayCommand(param => Utility.DiskImageCache.Instance.Clear());
				}
				return _clearCacheCommand;
			}
			set
			{
				_clearCacheCommand = value;
				NotifyPropertyChanged(nameof(ClearCacheCommand));
			}
		}

		public ICommand AddDirCommand
		{
			get
			{
				if (_addDirCommand == null)
				{
					_addDirCommand = new RelayCommand(
						param => AddDir(SelectedTreeDir), 
						param => CanTrackDirectory != null ? CanTrackDirectory(SelectedTreeDir) : SelectedTreeDir != null);
				}
				return _addDirCommand;
			}
			set
			{
				_addDirCommand = value;
				NotifyPropertyChanged(nameof(AddDirCommand));
			}
		}

		public ICommand RemoveDirCommand
		{
			get
			{
				if (_removeDirCommand == null)
				{
					_removeDirCommand = new RelayCommand(param => RemoveDir(SelectedDir), param => SelectedDir != null);
				}
				return _removeDirCommand;
			}
			set
			{
				_removeDirCommand = value;
				NotifyPropertyChanged(nameof(RemoveDirCommand));
			}
		}

		public string SelectedDir
		{
			get
			{
				return _selectedDir;
			}
			set
			{
				_selectedDir = value;
				NotifyPropertyChanged(nameof(SelectedDir));
			}
		}

		public string SelectedTreeDir
		{
			get
			{
				return _selectedTreeDir;
			}
			set
			{
				_selectedTreeDir = value;
				NotifyPropertyChanged(nameof(SelectedTreeDir));
			}
		}

		#endregion Properties

		public SettingsViewModel(Converters.ImageConverter converter, List<ImageListViewModel> imagesVMs)
		{
			_converter = converter;
			_imagesVMs = imagesVMs ?? new List<ImageListViewModel>();
			_settings = Properties.Settings.Default;
			ImagesPerPage = _settings.ImagesPerPage;
			IsPageSizeLimited = _settings.IsPageSizeLimited;
			IsRamCacheEnabled = _settings.IsRamCacheEnabled;
			IsDiskCacheEnabled = _settings.IsRamCacheEnabled;
			TrackedDirs = new ObservableCollection<string>();
			if (_settings.TrackedDirectories == null)
			{
				_settings.TrackedDirectories = new System.Collections.Specialized.StringCollection();
			}
			else
			{
				foreach (var dir in _settings.TrackedDirectories)
				{
					TrackedDirs.Add(dir);
				}
			}
		}

		private bool RemoveDir(string dirName)
		{
			if (!TrackedDirs.Contains(dirName))
			{
				return false;
			}
			_settings.TrackedDirectories.Remove(dirName);
			_settings.Save();
			TrackedDirs.Remove(dirName);
			return true;
		}

		private bool AddDir(string dirName)
		{
			if (TrackedDirs.Contains(dirName))
			{
				return false;
			}
			_settings.TrackedDirectories.Add(dirName);
			_settings.Save();
			TrackedDirs.Add(dirName);
			return true;
		}

		#region INotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected void NotifyPropertyChanged(string propertyName)
		{
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion
	}
}
