﻿using ImageManager.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ImageManager.ViewModels
{
	public class TagPickerViewModel : INotifyPropertyChanged, IDataErrorInfo
	{
		private string _tagTitle;
		private TagViewModel _selectedTag;
		private TagViewModel _lastPickedTag;
		private IEnumerable<TagViewModel> _matchingTags;
		private ObservableCollection<TagViewModel> _tags;
		private ObservableCollection<TagViewModel> _pickedTags;
		private ICommand _addTagCommand;
		private System.Windows.Threading.DispatcherTimer _selectionClearTimer;

		private Func<string, TagViewModel> _addNewTag;

		public event EventHandler TagPicked;
		public event EventHandler TagUnpicked;

		#region Properties

		public bool IsTagAutoDeselectEnabled { get; set; }

		public string TagTitle
		{
			get
			{
				return _tagTitle;
			}
			set
			{
				_tagTitle = value;
				Update();
				NotifyPropertyChanged(nameof(TagTitle));
			}
		}

		public IEnumerable<TagViewModel> MatchingTags
		{
			get
			{
				return _matchingTags;
			}
			private set
			{
				_matchingTags = value;
				NotifyPropertyChanged(nameof(MatchingTags));
			}
		}

		public ObservableCollection<TagViewModel> AllTags
		{
			get
			{
				return _tags;
			}
			private set
			{
				if (_tags != null)
				{
					_tags.CollectionChanged -= AllTags_CollectionChanged;
				}
				_tags = value;
				if (_tags != null)
				{
					_tags.CollectionChanged += AllTags_CollectionChanged;
				}
				NotifyPropertyChanged(nameof(AllTags));
			}
		}

		public ObservableCollection<TagViewModel> PickedTags
		{
			get
			{
				return _pickedTags;
			}
			set
			{
				_pickedTags = value;
				LastPickedTag = null;
				NotifyPropertyChanged(nameof(PickedTags));
			}
		}

		public TagViewModel SelectedTag
		{
			get
			{
				return _selectedTag;
			}
			set
			{
				_selectedTag = value;
				PickTag(_selectedTag);
				NotifyPropertyChanged(nameof(SelectedTag));
				if (IsTagAutoDeselectEnabled)
				{
					_selectionClearTimer.Start();
				}
			}
		}

		public TagViewModel LastPickedTag
		{
			get
			{
				return _lastPickedTag;
			}
			private set
			{
				_lastPickedTag = value;
				NotifyPropertyChanged(nameof(LastPickedTag));
			}
		}

		public ICommand AddTagCommand
		{
			get
			{
				if (_addTagCommand == null)
				{
					_addTagCommand = new Commands.RelayCommand(obj =>
					{
						var tag = _addNewTag?.Invoke(TagTitle);
						PickTag(tag);
					});
				}
				return _addTagCommand;
			}
			set
			{
				_addTagCommand = value;
				NotifyPropertyChanged(nameof(AddTagCommand));
			}
		}

		#endregion

		public TagPickerViewModel(ObservableCollection<TagViewModel> tags, Func<string, TagViewModel> addNewTag)
		{
			AllTags = tags;
			PickedTags = new ObservableCollection<TagViewModel>();
			TagTitle = null;
			_addNewTag = addNewTag;
			IsTagAutoDeselectEnabled = true;
			_selectionClearTimer = new System.Windows.Threading.DispatcherTimer();
			_selectionClearTimer.Tick += (s, e) => { SelectedTag = null; _selectionClearTimer.Stop(); };
			_selectionClearTimer.Interval = TimeSpan.FromMilliseconds(300);
		}

		public void PickTag(TagViewModel tag)
		{
			if (tag != null)
			{
				var addingToPicked = _pickedTags != null && !_pickedTags.Contains(tag);
				if (addingToPicked)
				{
					_pickedTags.Add(tag);
				}
				if (addingToPicked || _pickedTags == null)
				{
					LastPickedTag = tag;
					TagPicked?.Invoke(this, new EventArgs());
				}
			}
		}

		public void UnpickTag(TagViewModel tag)
		{
			bool unpicked = false;
			if (_pickedTags != null)
			{
				unpicked = _pickedTags.Remove(tag);
			}
			else if (_lastPickedTag != null)
			{
				LastPickedTag = null;
				unpicked = true;
			}
			if (unpicked)
			{
				TagUnpicked?.Invoke(this, new EventArgs());
			}
		}

		public void UnpickAll()
		{
			if (_pickedTags != null)
			{
				_pickedTags.Clear();
			}
		}

		public void Update()
		{
			MatchingTags = GetMatchingTags(TagTitle);
		}

		private IEnumerable<TagViewModel> GetMatchingTags(string text)
		{
			if (string.IsNullOrEmpty(text))
			{
				return _tags;
			}
			return _tags.Where(t => t.Title.StartsWith(text));
		}

		private void AllTags_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			Update();
		}

		#region IDataErrorInfo

		public string Error
		{
			get;
			private set;
		}

		public string this[string name]
		{
			get
			{
				string result = null;
				switch (name)
				{
					case nameof(TagTitle):
						if (!string.IsNullOrEmpty(TagTitle) && !Tag.IsTitleValid(TagTitle))
						{
							result = "Incorrect title.";
						}
						break;
				}
				Error = result;
				return result;
			}
		}

		#endregion

		#region INotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected void NotifyPropertyChanged(string propertyName)
		{
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion
	}
}
