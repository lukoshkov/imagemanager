﻿using ImageManager.Core;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ImageManager.ViewModels
{
	public class TagViewModel : INotifyPropertyChanged, IDataErrorInfo
	{
		private static Dictionary<Tag, TagViewModel> _createdVMs = new Dictionary<Tag, TagViewModel>();

		private Tag _tag;
		private string _title;
		private ObservableCollection<TagViewModel> _relatedTags;

		#region Properties

		public Tag Tag
		{
			get
			{
				return _tag;
			}
			private set
			{
				_tag = value;
				if (_tag != null)
				{
					Title = _tag.Title;
					RelatedTags = new ObservableCollection<TagViewModel>(Cast(_tag.RelatedTags));
				}
				else
				{
					Title = string.Empty;
					RelatedTags = new ObservableCollection<TagViewModel>();
				}
				NewTitle = Title;
				NotifyPropertyChanged(nameof(Tag));
			}
		}

		public string Title
		{
			get
			{
				return Tag.Title;
			}
			private set
			{
				Tag.Title = value;
				NotifyPropertyChanged(nameof(Title));
				NotifyPropertyChanged(nameof(IsTitleChanged));
			}
		}

		public string NewTitle
		{
			get
			{
				return _title;
			}
			set
			{
				_title = value;
				NotifyPropertyChanged(nameof(NewTitle));
				NotifyPropertyChanged(nameof(IsTitleChanged));
			}
		}

		public bool IsTitleChanged
		{
			get
			{
				return Tag == null ? false : !string.Equals(Title, NewTitle);
			}
		}

		public bool AreRelatedTagsChanged
		{
			get
			{
				if (Tag == null)
				{
					return false;
				}
				if (RelatedTags.Count != Tag.RelatedTags.Count)
				{
					return true;
				}
				List<Tag> added, deleted;
				Utility.CollectionUtil.CompareCollections(Cast(RelatedTags), Tag.RelatedTags, out added, out deleted);
				return added.Count > 0 || deleted.Count > 0;
			}
			private set
			{
				NotifyPropertyChanged(nameof(AreRelatedTagsChanged));
			}
		}

		public ObservableCollection<TagViewModel> RelatedTags
		{
			get
			{
				return _relatedTags;
			}
			private set
			{
				_relatedTags = value;
				NotifyPropertyChanged(nameof(RelatedTags));
				AreRelatedTagsChanged = true;
			}
		}

		#endregion

		private TagViewModel()
		{
		}

		public static TagViewModel GetViewModel(Tag tag)
		{
			if (tag == null)
			{
				return null;
			}
			TagViewModel tvm;
			if (!_createdVMs.TryGetValue(tag, out tvm))
			{
				tvm = new TagViewModel();
				_createdVMs.Add(tag, tvm);
				tvm.Tag = tag;
			}
			return tvm;
		}

		public static IEnumerable<TagViewModel> Cast(IEnumerable<Tag> tags)
		{
			return tags.Select(t => GetViewModel(t));
		}

		public static IEnumerable<Tag> Cast(IEnumerable<TagViewModel> tags)
		{
			return tags.Select(t => t.Tag);
		}

		public void ApplyChanges()
		{
			Title = NewTitle;
			_tag.RelatedTags.Clear();
			_tag.RelatedTags.AddRange(RelatedTags.Select(tvm => tvm.Tag));
			AreRelatedTagsChanged = false;
		}

		public void CancelChanges()
		{
			Title = _tag.Title;
		}

		public List<TagViewModel> GetAllRelatedTags()
		{
			var found = new HashSet<TagViewModel>();
			var result = new List<TagViewModel>();
			found.Add(this);
			result.Add(this);
			var index = 0;
			while (index < result.Count)
			{
				foreach (var tvm in result[index].RelatedTags)
				{
					if (!found.Contains(tvm))
					{
						found.Add(tvm);
						result.Add(tvm);
					}
				}
				index++;
			}
			return result;
		}

		#region IDataErrorInfo

		public string Error
		{
			get;
			private set;
		}

		public string this[string name]
		{
			get
			{
				string result = null;
				switch (name)
				{
					case nameof(NewTitle):
						if (!Tag.IsTitleValid(NewTitle))
						{
							result = "Incorrect title.";
						}
						break;
				}
				Error = result;
				return result;
			}
		}

		#endregion

		#region INotifyPropertyChanged

		public event PropertyChangedEventHandler PropertyChanged = delegate { };

		protected void NotifyPropertyChanged(string propertyName)
		{
			PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
		}

		#endregion
	}
}
