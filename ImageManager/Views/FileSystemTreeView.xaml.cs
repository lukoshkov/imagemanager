﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImageManager.Views
{
	/// <summary>
	/// Interaction logic for FileSystemTreeView.xaml
	/// </summary>
	public partial class FileSystemTreeView : UserControl
	{
		public static readonly DependencyProperty SelectedDirectoryProperty;

		private TreeView _tree;

		public string SelectedDirectory
		{
			get
			{
				return (string)GetValue(SelectedDirectoryProperty);
			}
			set
			{
				SetValue(SelectedDirectoryProperty, value);
			}
		}

		static FileSystemTreeView()
		{
			var metadata = new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.None);
			SelectedDirectoryProperty = DependencyProperty.Register("SelectedDirectory", typeof(string), typeof(FileSystemTreeView), metadata);
		}

		public FileSystemTreeView()
		{
			InitializeComponent();
			_tree = Content as TreeView;
			InitDirsTree();
		}

		private void InitDirsTree()
		{
			_tree.Items.Clear();
			foreach (var drive in System.IO.DriveInfo.GetDrives())
			{
				var item = new TreeViewItem();
				item.Tag = drive.Name;
				item.Header = drive.ToString();
				item.Items.Add("*");
				_tree.Items.Add(item);
			}
		}

		private void TreeView_Expanded(object sender, RoutedEventArgs e)
		{
			var item = (TreeViewItem)e.OriginalSource;
			item.Items.Clear();
			var dir = GetDirectoryInfo(item);
			try
			{
				foreach (var subDir in dir.GetDirectories())
				{
					if (!subDir.Attributes.HasFlag(System.IO.FileAttributes.Hidden))
					{
						TreeViewItem newItem = new TreeViewItem();
						newItem.Tag = subDir.FullName;
						newItem.Header = subDir.Name;
						newItem.Items.Add("*");
						item.Items.Add(newItem);
					}
				}
			}
			catch
			{ }
		}

		private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
		{
			SelectedDirectory = GetDirectoryInfo((TreeViewItem)_tree.SelectedItem).FullName;
		}

		private static System.IO.DirectoryInfo GetDirectoryInfo(TreeViewItem item)
		{
			var dirName = item.Tag as string;
			return new System.IO.DirectoryInfo(dirName);
		}
	}
}
