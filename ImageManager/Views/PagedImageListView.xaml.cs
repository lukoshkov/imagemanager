﻿using ImageManager.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ImageManager.Views
{
	/// <summary>
	/// Interaction logic for PagedImageListView.xaml
	/// </summary>
	public partial class PagedImageListView : UserControl
	{
		public event SelectionChangedEventHandler SelectionChanged;

		public PagedImageListView()
		{
			InitializeComponent();
		}

		private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			SelectionChanged?.Invoke(sender, e);
		}
	}
}
